package assign.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import assign.domain.Meeting;

public class MeetingCountServiceImpl implements MeetingCountService {

	final static String BASE_URL = "http://eavesdrop.openstack.org/meetings/";
	
	public MeetingCountServiceImpl(){
		
	}
			
	public List<Meeting> getMeetingCounts(){
		List<Meeting> meetings = getMeetingNames();
		for(int i = 0; i < meetings.size(); i++){
			Meeting m = meetings.get(i);
			int count = getMeetingCountForOneMeeting(m.getName());
			m.setCount(count);
		}
		return meetings;
	}
	
	
	private int getMeetingCountForOneMeeting(String name) {
		int result = 0;
		String url = BASE_URL+name+"/";
		Document doc;
		
		Map<String,List<String>> data = new HashMap<String,List<String>>();
		try {
			
			// need http protocol
			doc = Jsoup.connect(url).get();
	 
	 
			// get all links
			Elements outerLinks = doc.select("a[href]");
			for(int i = 5; i < outerLinks.size(); i++){
				Element ele = outerLinks.get(i); 
				String year = ele.attr("href");
				doc = Jsoup.connect(url+year).get();
				Elements innerLinks = doc.select("a[href]");
				for(int j = 5; j < innerLinks.size(); j++){
					String link = innerLinks.get(j).attr("href");
					int index1 = link.indexOf(".");
					int index2 = link.indexOf(".",index1+1);
					int index3 = link.indexOf(".",index2+1);
					String prefix = link.substring(0,index3);
					List<String> a = data.get(prefix);
					if(a == null){
						ArrayList<String> l = new ArrayList<String>();
						l.add(link);
						data.put(prefix, l);
					}
					else{
						a.add(link);
						data.put(prefix,a);
					}
					
				}
			}
	 
		} catch (IOException e) {
			
		}
		
		List<String> longestLinks = new ArrayList<String>();
		for(String s : data.keySet()){
			List<String> l = data.get(s);
			String longest = selectLongest(l);
			longestLinks.add(longest);
		}
		
		return longestLinks.size();
	}
	
	private String selectLongest(List<String> l) {
		String longest = "";
		for(int i = 0; i < l.size(); i++){
			String s = l.get(i);
			if(s.length() > longest.length())
				longest = s;
		}
		return longest;
	}

	private List<Meeting> getMeetingNames(){
		List<Meeting> meetings = new ArrayList<Meeting>();
		Document doc;
		
		try {
			
			// need http protocol
			doc = Jsoup.connect(BASE_URL).get();
	 
	 
			// get all links
			Elements links = doc.select("a[href]");
			for(int i = 5; i < links.size(); i++){
				Element ele = links.get(i); 
				String mname = ele.attr("href");
				mname = mname.substring(0,mname.length()-1);
				Meeting m = new Meeting();
				m.setName(mname);
				meetings.add(m);
			}
	 
		} catch (IOException e) {
			
		}
		return meetings;
	}
	
}
