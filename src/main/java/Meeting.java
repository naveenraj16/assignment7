package assign.domain;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



@XmlRootElement(name="meeting")
@XmlType(propOrder={"name","count"})
public class Meeting {
   
    private String name;
    private int count;

   
    public Meeting() {
       
    }
   
    public Meeting(String name, int count) {
        this.name = name;
        this.count = count;
    }

    @XmlElement(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name="count")
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
   
   
}

