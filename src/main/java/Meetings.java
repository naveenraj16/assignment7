package assign.domain;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "meetings")
public class Meetings {

	List<Meeting> meetings;
	
	public Meetings(){
		
	}
	
	@XmlElement(name = "meeting")
	public List<Meeting> getMeetings() {
		return meetings;
	}

	public void setMeetings(List<Meeting> meetings) {
		this.meetings = meetings;
	}
	
	
}
