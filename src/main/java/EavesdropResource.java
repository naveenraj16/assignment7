package assign.resources;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import assign.domain.Meeting;
import assign.domain.Meetings;
import assign.services.MeetingCountService;
import assign.services.MeetingCountServiceImpl;


@Path("/myeavesdrop")
public class EavesdropResource {
	
	
	public EavesdropResource(@Context ServletContext servletContext) {		
		
	}

@GET
@Path("/helloworld")
@Produces("text/html")
public String helloWorld() {
return "Hello world";
}


@GET
@Path("/meetingCounts")
@Produces("application/xml")
public Response getMeetingCounts() throws Exception{
	MeetingCountService mcs = new MeetingCountServiceImpl();
	List<Meeting> ms = mcs.getMeetingCounts();
	final Meetings meetings = new Meetings();
	meetings.setMeetings(ms);
	if(ms != null){
		StreamingOutput so = new StreamingOutput() {
			public void write(OutputStream outputStream) throws IOException, WebApplicationException {
				outputMeetingCounts(outputStream, meetings);
			}
		};
		Response response = Response.ok(so).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	}
	else{
		Response response =  Response.status(Response.Status.NOT_FOUND).build();
		response.getHeaders().putSingle("Access-Control-Allow-Origin", "*");
		return response;
	}
	
}




protected void outputMeetingCounts(OutputStream os, Meetings meetings) throws IOException {
	try {
		JAXBContext jaxbContext = JAXBContext.newInstance(Meetings.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(meetings, os);
} catch (JAXBException jaxb) {
	jaxb.printStackTrace();
	throw new WebApplicationException();
}
}


}