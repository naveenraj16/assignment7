package assign.services;

import java.util.List;

import assign.domain.Meeting;

public interface MeetingCountService {

	public List<Meeting> getMeetingCounts();
}
