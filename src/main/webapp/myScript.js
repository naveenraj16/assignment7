/**
 * 
 */

var xmlhttp;
function getMeetingCounts(){
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	}
	
	else  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	var url = "http://localhost:8080/assignment7/myeavesdrop/meetingCounts";
	xmlhttp.open("GET",url,true);
	xmlhttp.send();
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("myDiv").innerHTML=createHtmlTable(xmlhttp.responseXML);
	    }
		else{
			alert(xmlhttp.responseText);
		}
	};
}

function createHtmlTable(meetings){
	var htmlString = "<table border=\"2\"><tr><th>Meeting Name</th><th>Numer of Meetings</th></tr>";
	var m=meetings.getElementsByTagName("meeting");
	for (i=0;i<m.length;i++){
		var mname = m[i].getElementsByTagName("name")[0].childNodes[0].nodeValue;
		var count = m[i].getElementsByTagName("count")[0].childNodes[0].nodeValue;
		htmlString += "<tr><td>";
		htmlString += mname;
		htmlString += "</td><td>";
		htmlString += count;
		htmlString += "</td></tr>";
	 }
	 htmlString += "</table>";
	 return htmlString;
}
